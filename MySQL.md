## Servidor MySQL

Instrucciones para instalar el cliente de MySQL en Fedora 36

```shell
# Instalar cliente MySQL en Fedora (36)
sudo dnf update -y

sudo dnf search mysql | grep client

sudo dnf search community-mysql
# Instalar el cliente que aparece como:
# community-mysql.x86_64 : MySQL client programs and shared libraries
```

