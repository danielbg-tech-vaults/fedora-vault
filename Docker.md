# Comandos para utilizar Docker en Fedora Core

## Instalación en Fedora Core 36
```shell
sudo dnf update -y

sudo dnf -y install dnf-plugins-core

sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Referencia para la instalación: https://docs.docker.com/engine/install/fedora/

## Iniciar el servicio
```shell
# Opción 1
sudo service docker start

# Opción 2
sudo systemctl start docker
```

## Verificar la instalación con el contedor hello-world
```shell
sudo docker run hello-world
```

## Detener el servicio
```shell
sudo service docker stop
```

## Iniciar un contenedor creado previamente
Revisar los procesos creados antes de iniciar alguno
```shell
sudo docker ps -a
```

Iniciar y detener uno o varios procesos
```shell
# Iniciar
# sudo docker start <servicio-1> <servicio-2> ... <servicio-n>
sudo docker start mailhog gitlab

# Detener
# sudo docker start <servicio-1> <servicio-2> ... <servicio-n>
sudo docker stop mailhog gitlab
```

