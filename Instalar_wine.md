# Instalar wine en Fedora 36

## Instrucciones

```shell
# Actualizar
sudo dnf upgrade

# Agrega el repositorio de Wine
sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/36/winehq.repo

# Instalar
sudo dnf install winehq-devel

# Verificar la instalación
wine --version

# Configurar para windows 10
wine winecfg
```

## Referencias
https://wine.htmlvalidator.com/install-wine-on-fedora-workstation-36.html