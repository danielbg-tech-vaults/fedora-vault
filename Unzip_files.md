Instalación del comando

```shell
sudo dnf update
sudo dnf install -y unzip
```

Ejemplo de uso:

```shell
unzip ejemplos.zip
```

Descomprimirá el archivo `ejemplos.zip` creando la estructura original del archivo empaquetado.

Para especificar un directorio diferente

```shell
unzip ejemplos.zip -d path/to/different/directory
```

**Referencias**
https://linuxize.com/post/how-to-unzip-files-in-linux/